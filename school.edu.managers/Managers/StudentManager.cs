﻿using school.edu.managers.Interfaces;
using school.edu.model.Interfaces;
using school.edu.model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace school.edu.managers.Managers
{
    public class StudentManager : IStudentManager
    {
        private readonly IStudentRepository _studentRepository;

        public StudentManager(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }

        public async Task<Student> CreateStudent(Student student)
        {
            var createdStudent = await _studentRepository.CreateStudent(student).ConfigureAwait(false);

            return createdStudent;
        }

        public List<Student> GetStudents()
        {
            return _studentRepository.GetStudents();
        }

        public async Task<Student> GetStudent(int id)
        {
            var student = await _studentRepository.GetStudent(id).ConfigureAwait(false);

            return student;
        }

        public async Task<Student> UpdateStudent(Student student)
        {
            var updatedStudent = await _studentRepository.UpdateStudent(student).ConfigureAwait(false);

            return updatedStudent; ;
        }

        public Student RemoveStudent(Student student)
        {
            return _studentRepository.RemoveStudent(student);
        }
    }
}
