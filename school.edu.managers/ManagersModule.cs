﻿using Autofac;
using school.edu.managers.Interfaces;
using school.edu.managers.Managers;

namespace school.edu.managers
{
    public class ManagersModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Register managers
            builder.RegisterType<StudentManager>()
                    .As<IStudentManager>();
        }
    }
}
