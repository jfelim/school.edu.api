﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using school.edu.model;
using Microsoft.OpenApi.Models;
using school.edu.managers;
using Microsoft.AspNetCore.Mvc;
using FluentValidation.AspNetCore;

namespace school.edu.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddFluentValidation();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Test API",
                    Version = "v1"
                });
            });
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterModule(new ModelModule());
            containerBuilder.RegisterModule(new ManagersModule());
            //services.AddDbContext<SampleDbContext>(options =>
            //   options.UseSqlite(@"Data Source=C:\ProjectOrbit\school.edu.api\school.edu.model\sample.db3"));
            containerBuilder.Populate(services);
            var container = containerBuilder.Build();
            return new AutofacServiceProvider(container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(x => x.SwaggerEndpoint("/swagger/v1/swagger.json", "Test API"));
        }
    }
}
