﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Settings.Configuration;
using Serilog.Sinks.ApplicationInsights.Sinks.ApplicationInsights.TelemetryConverters;
using System;
using System.IO;

namespace school.edu.api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                                .AddJsonFile("appsettings.json", false, true)
                                .AddEnvironmentVariables()
                                .Build();

            var loggerConfiguration = new LoggerConfiguration()
                                      .ReadFrom.Configuration(configuration, ConfigurationAssemblySource.UseLoadedAssemblies)
                                      .Enrich.FromLogContext()
                                      .Enrich.WithProperty("ServiceName", configuration["ApplicationInsights:ServiceName"])
                                      .WriteTo.ApplicationInsights(configuration["ApplicationInsights:InstrumentationKey"], new TraceTelemetryConverter(),
                                          LogEventLevel.Information);

            Log.Logger = loggerConfiguration.CreateLogger();

            try
            {
                Log.Information("Starting up");
                CreateHostBuilder(args);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Application start-up failed");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static void CreateHostBuilder(string[] args)
        {            
            var host = new WebHostBuilder()
                .UseSerilog()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();
            host.Run();
        }
    }
}
