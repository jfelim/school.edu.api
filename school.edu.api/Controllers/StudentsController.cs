﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using school.edu.managers.Interfaces;
using school.edu.model.Models;

namespace school.edu.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly IStudentManager _studentsManager;

        public StudentsController(IStudentManager studentsManager)
        {
            _studentsManager = studentsManager;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Student>> GetStudents()
        {
            return Ok(_studentsManager.GetStudents());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Student>> GetStudent(int id)
        {
            return Ok(await _studentsManager.GetStudent(id).ConfigureAwait(false));
        }

        [HttpPost]
        public async Task<ActionResult<Student>> CreateStudent([FromBody] Student student)
        {
            return Ok(await _studentsManager.CreateStudent(student).ConfigureAwait(false));
        }

        [HttpPut]
        public async Task<ActionResult<Student>> Put([FromBody] Student student)
        {
            return Ok(await _studentsManager.UpdateStudent(student).ConfigureAwait(false));
        }

        [HttpPost("remove")]
        public IActionResult Delete([FromBody] Student student)
        {
            _studentsManager.RemoveStudent(student);

            return NoContent();
        }
    }
}
