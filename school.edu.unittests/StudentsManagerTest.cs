﻿using AutoFixture;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using school.edu.managers.Interfaces;
using school.edu.managers.Managers;
using school.edu.model.Interfaces;
using school.edu.model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    public class StudentsManagerTest
    {
        private IStudentManager _target;
        private IFixture _fixture;
        private Mock<IStudentRepository> _repositoryMock;

        [SetUp]
        public void Setup()
        {
            _repositoryMock.Reset();            
        }

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            _fixture = new Fixture();
            _repositoryMock = new Mock<IStudentRepository>();
            _target = new StudentManager(_repositoryMock.Object);
        }

        [Test]
        public async Task CreateStudent_Ok()
        {
            var student = _fixture.Create<Student>();
            student.Id = 1;
            student.Age = 25;

            _repositoryMock.Setup(x => x.CreateStudent(It.IsAny<Student>())).ReturnsAsync(student);

            var result = await _target.CreateStudent(student);

            result.Should().NotBeNull();
            result.Id.Should().Be(1);
            result.Age.Should().Be(25);
            result.Username.Should().NotBeNullOrEmpty();
            result.FirstName.Should().NotBeNullOrEmpty();
            result.LastName.Should().NotBeNullOrEmpty();
            result.Career.Should().NotBeNullOrEmpty();
        }

        [Test]
        public void GetStudents_Ok()
        {
            var student1 = _fixture.Create<Student>();
            var student2 = _fixture.Create<Student>();
            var students = new List<Student>() { student1, student2 };
            _repositoryMock.Setup(x => x.GetStudents()).Returns(students);

            var result = _target.GetStudents();

            result.Should().NotBeNull();
            result.Count.Should().Be(2);
        }

        [Test]
        public async Task UpdateStudent_Ok()
        {
            var student = _fixture.Create<Student>();                        
            _repositoryMock.Setup(x => x.UpdateStudent(student)).ReturnsAsync(student);

            var result = await _target.UpdateStudent(student);

            result.Should().NotBeNull();
            result.Id.Should().Be(student.Id);
            result.Username.Should().Be(student.Username);
            result.FirstName.Should().Be(student.FirstName);
            result.LastName.Should().Be(student.LastName);
            result.Career.Should().Be(student.Career);
            result.Age.Should().Be(student.Age);
        }

        [Test]
        public void RemoveStudent_Ok()
        {
            var student = _fixture.Create<Student>();
            _repositoryMock.Setup(x => x.RemoveStudent(student)).Returns(student);

            var result = _target.RemoveStudent(student);

            result.Should().NotBeNull();
            result.Id.Should().Be(student.Id);
        }
    }
}