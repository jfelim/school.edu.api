using AutoFixture;
using FluentAssertions;
using FluentValidation;
using FluentValidation.Results;
using Moq;
using NUnit.Framework;
using school.edu.model.Interfaces;
using school.edu.model.Models;
using school.edu.model.Validation;
using System.Linq;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    public class StudentValidationTests
    {
        private IValidator<Student> _target;
        private IFixture _fixture;
        private Mock<IStudentRepository> _repositoryMock;

        [SetUp]
        public void Setup()
        {
            _repositoryMock.Reset();

            var student = _fixture.Create<Student>();
            _repositoryMock.Setup(x => x.CreateStudent(It.IsAny<Student>())).ReturnsAsync(student);
        }

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            _fixture = new Fixture();            
            _repositoryMock = new Mock<IStudentRepository>();
            _target = new StudentValidator();
        }

        [Test]
        public async Task Validate_Id_Fails()
        {
            var student = _fixture.Create<Student>();
            student.Id = -1;
            var result = await _target.ValidateAsync(student);

            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
            result.Errors.First().Should()
                .Match<ValidationFailure>(x => x.ErrorMessage.Equals("'Id' must be greater than '0'."));
        }

        [Test]
        public async Task Validate_Age_Fails()
        {
            var student = _fixture.Create<Student>();
            student.Age = -1;
            var result = await _target.ValidateAsync(student);

            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
            result.Errors.First().Should()
                .Match<ValidationFailure>(x => x.ErrorMessage.Equals("'Age' must be greater than '0'."));
        }

        [Test]
        public async Task Validate_Usermame_Empty_Fails()
        {
            var student = _fixture.Create<Student>();
            student.Username = string.Empty;
            var result = await _target.ValidateAsync(student);

            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
            result.Errors.First().Should()
                .Match<ValidationFailure>(x => x.ErrorMessage.Equals("'Username' must not be empty."));
        }

        [Test]
        public async Task Validate_FirstName_Empty_Fails()
        {
            var student = _fixture.Create<Student>();
            student.FirstName = string.Empty;
            var result = await _target.ValidateAsync(student);

            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
            result.Errors.First().Should()
                .Match<ValidationFailure>(x => x.ErrorMessage.Equals("'First Name' must not be empty."));
        }

        [Test]
        public async Task Validate_LastName_Empty_Fails()
        {
            var student = _fixture.Create<Student>();
            student.LastName = string.Empty;
            var result = await _target.ValidateAsync(student);

            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
            result.Errors.First().Should()
                .Match<ValidationFailure>(x => x.ErrorMessage.Equals("'Last Name' must not be empty."));
        }

        [Test]
        public async Task Validate_Carreer_Empty_Fails()
        {
            var student = _fixture.Create<Student>();
            student.Career = string.Empty;
            var result = await _target.ValidateAsync(student);

            result.IsValid.Should().BeFalse();
            result.Errors.Should().HaveCount(1);
            result.Errors.First().Should()
                .Match<ValidationFailure>(x => x.ErrorMessage.Equals("'Career' must not be empty."));
        }
    }
}