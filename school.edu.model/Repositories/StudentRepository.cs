﻿using Microsoft.EntityFrameworkCore;
using school.edu.model.Interfaces;
using school.edu.model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace school.edu.model.Repositories
{
    public class StudentRepository : IStudentRepository
    {
        private readonly SampleDbContext _dbContext;

        public StudentRepository(SampleDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Student> CreateStudent(Student student)
        {
            _dbContext.Entry(student).State = EntityState.Added;
            var createdStudent = await _dbContext.Student.AddAsync(student).ConfigureAwait(false);
            await _dbContext.SaveChangesAsync();

            return createdStudent.Entity;
        }

        public List<Student> GetStudents()
        {
           List<Student> students = new List<Student>();
           foreach(var s in _dbContext.Student)
           {
                students.Add(s);
           }

           return students;
        }

        public async Task<Student> GetStudent(int id)
        {
            var student = await _dbContext.Student.FindAsync(id).ConfigureAwait(false);

            return student;
        }

        public async Task<Student> UpdateStudent(Student student)
        {
            _dbContext.Entry(student).State = EntityState.Modified;
            var updateStudent =_dbContext.Student.Update(student);
            await _dbContext.SaveChangesAsync();
            return updateStudent.Entity;
        }

        public Student RemoveStudent(Student student)
        {
            _dbContext.Entry(student).State = EntityState.Deleted;
            var removedStudent = _dbContext.Student.Remove(student);
            _dbContext.SaveChanges();

            return removedStudent.Entity;
        }
    }
}
