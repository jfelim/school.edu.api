﻿using Microsoft.EntityFrameworkCore;
using school.edu.model.Models;
using System;
using System.IO;
using System.Reflection;

namespace school.edu.model.Repositories
{
    public class SampleDbContext : DbContext
    {
        public DbSet<Student> Student { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory));
            optionsBuilder.UseSqlite($"Data Source={path}/sample.db3", options =>
            {
                options.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName);
            });
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {            
            modelBuilder.Entity<Student>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Username).IsRequired();
                entity.Property(e => e.FirstName).IsRequired();
                entity.Property(e => e.LastName).IsRequired();
                entity.Property(e => e.Age).IsRequired();
                entity.Property(e => e.Career).IsRequired();
            });
            base.OnModelCreating(modelBuilder);
        }
    }
}
