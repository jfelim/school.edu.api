﻿using school.edu.model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace school.edu.model.Interfaces
{
    public interface IStudentRepository
    {
        Task<Student> CreateStudent(Student student);
        List<Student> GetStudents();
        Task<Student> GetStudent(int id);
        Task<Student> UpdateStudent(Student student);
        Student RemoveStudent(Student student);
    }
}
