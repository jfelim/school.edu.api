﻿using Autofac;
using FluentValidation;
using school.edu.model.Interfaces;
using school.edu.model.Models;
using school.edu.model.Repositories;
using school.edu.model.Validation;

namespace school.edu.model
{
    public class ModelModule : Module
    {        
        protected override void Load(ContainerBuilder builder)
        {
            // Register DBContext
            builder.RegisterType<SampleDbContext>()
                .As<SampleDbContext>()
                .InstancePerLifetimeScope();

            // Register repositories
            builder.RegisterType<StudentRepository>()
                .As<IStudentRepository>()
                .InstancePerLifetimeScope();

            // Register validators
            builder.RegisterType<StudentValidator>()
                .As<IValidator<Student>>();

        }
    }
}
