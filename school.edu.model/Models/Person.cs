﻿using System;
using System.ComponentModel.DataAnnotations;

namespace school.edu.model.Models
{
    public class Person
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public Int64 Age { get; set; }
    }
}
