﻿using System.ComponentModel.DataAnnotations;

namespace school.edu.model.Models
{
    public class Student : Person
    {
        [Key]
        public long Id { get; set; }        
        [Required]
        public string Career { get; set; }
    }
}
